FROM nginx:1.15-alpine

RUN rm -rf /etc/nginx/conf.d/default.conf

COPY nginx.conf /etc/nginx/
COPY api.conf /etc/nginx/conf.d/

RUN echo "upstream php-upstream { server php:9001; }" > /etc/nginx/conf.d/upstream.conf

RUN addgroup -S www-data && adduser -D -g '' -G www-data www-data

EXPOSE 80
